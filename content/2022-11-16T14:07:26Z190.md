+++
 title = "Привет! На связи Podlodka Crew. Долгожданная премьера Podlodka Java Crew. 21 ноября вместе погрузимся в тему «Spring Framework»."
 date = 2022-11-16T14:07:26Z
 description = "%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82%21%20%D0%9D%D0%B0%20%D1%81%D0%B2%D1%8F%D0%B7%D0%B8%20Podlodka%20Crew.%20%D0%94%D0%BE%D0%BB%D0%B3%D0%BE%D0%B6%D0%B4%D0%B0%D0%BD%D0%BD%D0%B0%D1%8F%20%D0%BF%D1%80%D0%B5%D0%BC%D1%8C%D0%B5%D1%80%D0%B0%20Podlodka%20Java%20Crew.%2021%20%D0%BD%D0%BE%D1%8F%D0%B1%D1%80%D1%8F%20%D0%B2%D0%BC%D0%B5%D1%81%D1%82%D0%B5%20%D0%BF%D0%BE%D0%B3%D1%80%D1%83%D0%B7%D0%B8%D0%BC%D1%81%D1%8F%20%D0%B2%20%D1%82%D0%B5%D0%BC%D1%83%20%C2%ABSpring%20Framework%C2%BB."
 [extra]
 preview_img ="https://objectstorage.eu-frankfurt-1.oraclecloud.com/n/frctqqona9qo/b/bucket-20220204-1012/o/adminka/zqhcwo9989u1o4zqni63rlplyg3ituz3e4d33d0cadad-%D0%B4%D0%B6%D0%B0%D0%B2%D0%B0_%D0%BD%D1%8C%D1%8E.png"
 tags ="# #мероприятия"
+++
Привет! На связи Podlodka Crew. Долгожданная премьера Podlodka Java Crew. 21 ноября вместе погрузимся в тему «Spring Framework».

 
🤩 Мы уже анонсировали звёздного гостя — легендарного Java Champion Otavio Santana. 

Раскроем новые подробности программы:

♨️ Григорий Кошелев, Андрей Беляев и Алексей Абышев расскажут про альтернативы Spring — Micronaut и Quarkus. Чем они отличаются, нужно ли на них переходить, какие у них плюсы и минусы.
♨️ Проведём круглый стол про тестирование Spring. Вместе обсудим, как тестировать Spring-приложения, и разберём наглядные и интересные кейсы.

А ещё в программе:

🎙 Открытый микрофон. Все желающие смогут вместе с программным комитетом подготовить собственный доклад.
🔋 Java/Spring зарядка — ответим на нетривиальные вопросы по Java и Spring в формате квиза.

Podlodka Crew — это уникальный экспертный контент, интенсивы с морем нетворкинга и практических кейсов. 

😎 Промокод на скидку 500 руб: java_crew_1_LEACgd

👉 Купить билет и посмотреть расписание можно на сайте

Автор публикации: Podlodka Crew
  

 #мероприятия


https://podlodka.io/javacrew?utm_campaign=main_java_crew_1&utm_source=telegram&utm_medium=social&utm_content=Samara_IT_Community