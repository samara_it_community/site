+++
 title = "iOS разработчик"
 date = 2022-02-22T09:08:08Z
 description = "%23%D0%B2%D0%B0%D0%BA%D0%B0%D0%BD%D1%81%D0%B8%D1%8F%20%23iOS%20%23SWIFT%20%23remote"
 [extra]
 preview_img ="https://objectstorage.eu-frankfurt-1.oraclecloud.com/n/frctqqona9qo/b/bucket-20220204-1012/o/adminka/bgimzthg7xgsurgabgin7t9fllf4et1id091f4a6167f-Screenshot_2.jpg"
 tags ="# #вакансии"
+++
Вакансия iOS

 
Компания: DreamTeam
Формат работы: удаленно
Уровень позиции: Middle+
Вилка: от  $3500 до $5000 
Telegram @AmataItalia

DreamTeam ищет iOS разработчиков, с опытом работы с swift.

Небольшая команда, все работают удаленно.


🏄🏼‍♂️🏄🏼‍♂️ Требования:
-  опыт от 2-ух лет (коммерческий опыт)
- хорошее владение и понимание swift 
- опыт работы с CI/CD
- английский B1 (лучше B1+ и выше)

🏄🏼‍♂️🏄🏼‍♂️ Условия:
-  B2B договор
- адекватное руководство 

Контакты: @AmataItalia
Присылайте, пожалуйста, ваше резюме + github link 
Подробнее расскажу о компании и вакансии в ЛС

Автор публикации: Аня @AmataItalia
  

 #вакансии
