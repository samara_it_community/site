+++
 title = "International Women's Day 2022 EPAM + Intel"
 date = 2022-02-22T15:34:35Z
 description = "International%20Women%27s%20Day%202022%20EPAM%20%2B%20Intel"
 [extra]
 preview_img ="https://objectstorage.eu-frankfurt-1.oraclecloud.com/n/frctqqona9qo/b/bucket-20220204-1012/o/adminka/oyfiauidet6msn76xoyfiafsu0vptji5e9c5e415d764-600%D1%85300.png"
 tags ="# #мероприятия"
+++
International Women's Day 2022 EPAM + Intel

 
С 4 по 11 марта подключайтесь к серии мероприятий от EPAM и INTEL, где мы поговорим про старт карьеры в ИТ, лидерство, женские технические сообщества и о многом другом.

Спикеры из EPAM и INTEL ответят на ваши вопросы и поделятся карьерными лайфхаками. Не пропустите!

Автор публикации: EPAM Events Crew
  

 #мероприятия
