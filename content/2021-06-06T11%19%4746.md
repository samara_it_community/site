+++
 title = "Скептичный просмотр WWDC2021"
 date = 2021-06-06
 description = "%D0%91%D1%83%D0%B4%D0%B5%D0%BC%20%D1%81%D0%BC%D0%BE%D1%82%D1%80%D0%B5%D1%82%D1%8C%20%D0%BF%D1%80%D1%8F%D0%BC%D1%83%D1%8E%20%D1%82%D1%80%D0%B0%D0%BD%D1%81%D0%BB%D1%8F%D1%86%D0%B8%D1%8E%20%D0%B4%D0%BE%D1%81%D1%82%D0%B8%D0%B6%D0%B5%D0%BD%D0%B8%D0%B9%20%D0%BA%D0%B0%D0%BB%D0%B8%D1%84%D0%BE%D1%80%D0%BD%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D1%85%20%D1%83%D0%BC%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%20%D0%B8%20%D1%81%D0%BA%D0%BE%D1%80%D0%B1%D0%BD%D0%BE%20%D0%B2%D0%BE%D0%BD%D1%8F%D1%82%D1%8C%20%D0%BE%20%D1%82%D0%BE%D0%BC%2C%20%D0%BD%D0%B0%D1%81%D1%82%D0%BE%D0%BB%D1%8C%D0%BA%D0%BE%20%D1%83%D1%81%D0%BB%D0%BE%D0%B6%D0%BD%D0%B8%D1%82%D1%81%D1%8F%20%D0%BD%D0%B0%D1%88%D0%B0%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.%207%20%D0%B8%D1%8E%D0%BD%D1%8F%20%D0%BA%2020%3A00%2C%20%D0%B1%D0%B0%D1%80%20%22Community%22"
 [extra]
 preview_img ="/logo.jpg"
+++

Будем смотреть прямую трансляцию достижений калифорнийских умников и скорбно вонять о том, настолько усложнится наша работа. 7 июня к 20:00, бар "Community"
Hermesis



#мероприятия
https://trismegistus.timepad.ru/event/1670304/