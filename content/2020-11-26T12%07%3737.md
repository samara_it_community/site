+++
 title = "Вакансия Front-end Developer "
 date = 2020-11-26
 description = "Вакансия Front-end Developer "
+++

Вакансия Front-end Developer 
CQG, г. Самара

📌 Штаб-квартира CQG находится в Колорадо с офисами продаж и поддержки клиентов в Чикаго, Нью-Йорке, Денвере, Гленвуд-Спрингс, Лондоне, Франкфурте, Токио, Сиднее, Сингапуре, Москве и Китае. Миссия компании - создавать инновационные решения для индустрии финансового трейдинга. Уже более 30 лет CQG предоставляет рыночные данные и услуги ISV.

💡Задачи:
- Написание нового и поддержка уже существующего функционала клиент-серверных систем 
- Front-End разработка с использование технологий .NET, JavaScript, TypeScript, Angular 
- Back-end разработка с использование технологий ASP.NET MVC/WEBAPI, MSSQL 
- Тестирование кода 
- Написание MSTest, Selenium, karma, jasmine, protractor тестов

✅Что необходимо:
- Опыт разработки Web-приложений с использованием React, Angular, ASP.NET MVC/WEBAPI от 2х лет 
- Знание английского языка на уровне деловой переписки 
- Знание принципов ООП и паттернов проектирования 
- Знание теоретических основ баз данных, опыт разработки на T-SQL, опыт работы с MS SQL. 
- Хорошее знание .NET framework 
- Опыт разработки многопоточных приложений под Windows

🍰 Условия: 
- Работа в международной компании, которая уже 40 лет успешно лидирует на мировом рынке программных решений для трейдинга; 
- офис в Самаре с видом на Волгу; 
- полный соцпакет и мед. страховка со стоматологией на сотрудника и членов его семьи; 
- уютная кухня, где всегда есть вкусная еда, чай и кофе; 
- бесплатные занятия английским дважды в неделю в рабочее время; 
- дружелюбная и творческая атмосфера. Серьезно! 
 
📞8 927 016 96 74 
✉junina@cqg.com

#вакансии