+++
 title = "WWDC вместе с Ecwid"
 date = 2021-06-03
 description = "7%20%D0%B8%D1%8E%D0%BD%D1%8F%20%D0%BA%2020%3A00%20%D0%B2%20%D0%B1%D0%B0%D1%80%20%E2%80%9CCommunity%E2%80%9D%20%D1%80%D0%B5%D0%B1%D1%8F%D1%82%D0%B0%20%D0%B8%D0%B7%20%D0%AD%D0%BA%D0%B2%D0%B8%D0%B4%D0%B0%20%D0%BF%D1%80%D0%B8%D0%B3%D0%BB%D0%B0%D1%88%D0%B0%D1%8E%D1%82%20%D0%B2%D1%81%D0%B5%D1%85%20%D0%BD%D0%B5%D1%80%D0%B0%D0%B2%D0%BD%D0%BE%D0%B4%D1%83%D1%88%D0%BD%D1%8B%D1%85%20%D0%BA%20%D0%BC%D0%B8%D1%80%D1%83%20Apple%20%D0%BF%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80%D0%B5%D1%82%D1%8C%20%D0%BA%D0%B5%D0%B9%D0%BD%D0%BE%D1%83%D1%82%20WWDC%202021.%20%D0%9F%D0%BE%D0%BE%D0%B1%D1%81%D1%83%D0%B6%D0%B4%D0%B0%D1%82%D1%8C%20%D0%B7%D0%B0%20%D0%BA%D1%80%D1%83%D0%B6%D0%B5%D1%87%D0%BA%D0%BE%D0%B9%20%D0%BA%D1%80%D0%B0%D1%84%D1%82%D0%B0%20%D0%BD%D0%BE%D0%B2%D1%8B%D0%B5%20%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D0%B8%20iOS%2C%20iPadOS%2C%20macOS%2C%20watchOS%2C%20tvOS%2C%20%D0%B2%D0%B7%D0%B3%D0%BB%D1%8F%D0%BD%D1%83%D1%82%D1%8C%20%D0%BD%D0%B0%20%D0%BD%D0%BE%D0%B2%D1%8B%D0%B5%20MacBook%20Pro%2C%20%D0%B4%D0%B0%20%D0%B8%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%20%D0%BF%D0%BE%D0%BD%D1%8F%D1%82%D1%8C%20%22%D1%87%D1%82%D0%BE%20%D1%82%D0%B0%D0%BC%20%D0%B2%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%20%D0%BF%D1%80%D0%BE%D0%B8%D1%81%D1%85%D0%BE%D0%B4%D0%B8%D1%82%22."
 [extra]
 preview_img ="/logo.jpg"
+++

7 июня к 20:00 в бар “Community” ребята из Эквида приглашают всех неравнодушных к миру Apple посмотреть кейноут WWDC 2021. Пообсуждать за кружечкой крафта новые версии iOS, iPadOS, macOS, watchOS, tvOS, взглянуть на новые MacBook Pro, да и просто понять "что там вообще происходит".
Женя

jane@ecwid.com
https://t.me/janesycheva
#мероприятия
https://ecwid.timepad.ru/event/1660161/