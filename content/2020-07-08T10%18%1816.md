+++
 title = "Вакансия"
 date = 2020-07-08
 description = "Вакансия"
+++

Вакансия

Разработчик и Data Scientist, найдись! 
 
Если вы любите видео игры, классно пишите код и креативно мыслите, в лондонском стартапе по аналитике и прокачке скилов геймеров Game Academy открыты 2 вакансии: 
- Full-stack developer с развитием вплоть до CTO 
- Data Scientist делать ресерч и анализ мирового уровня по теме скилов геймеров. 
 
Работать удаленно в международной команде, поэтому нужен английский.
Подаваться можно напрямую на hh, по вопросам писать irina@gameacademy.co - основательница стартапа кстати из Самары!

https://hh.ru/employer/4732956

#вакансии