+++
 title = "SITQuest #1 - муниципальный"
 date = 2021-08-13
 description = "%D0%98%20%D1%81%D0%BD%D0%BE%D0%B2%D0%B0%20%D0%BA%D0%B2%D0%B5%D1%81%D1%82%20-%20%D0%BD%D0%B0%20%D1%8D%D1%82%D0%BE%D1%82%20%D1%80%D0%B0%D0%B7%20%D0%BC%D0%B0%D1%81%D1%88%D1%82%D0%B0%D0%B1%D0%BD%D0%B5%D0%B5%20%D0%B8%20%D1%81%D0%BB%D0%BE%D0%B6%D0%BD%D0%B5%D0%B5%2C%20%D0%BE%D1%82%D0%B2%D0%B0%D0%B6%D0%B8%D1%82%D0%B5%D1%81%D1%8C%20%D0%BB%D0%B8%20%D0%B2%D1%8B%20%D0%BF%D0%BE%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D1%82%D1%8C%20%D0%B2%20%D0%BD%D1%91%D0%BC%3F%20%D0%95%D1%81%D0%BB%D0%B8%20%D0%B4%D0%B0%20-%20%D1%82%D0%BE%20%D1%81%D0%BB%D0%B0%D0%B2%D0%B0%20%D0%B8%20%D1%81%D0%BE%D0%BA%D1%80%D0%BE%D0%B2%D0%B8%D1%89%D0%B0%20%D0%B6%D0%B4%D1%83%D1%82%20%D0%BF%D0%BE%D0%B1%D0%B5%D0%B4%D0%B8%D1%82%D0%B5%D0%BB%D0%B5%D0%B9%21"
 [extra]
 preview_img ="/logo.jpg"
+++

И снова квест - на этот раз масштабнее и сложнее, отважитесь ли вы поучаствовать в нём? Если да - то слава и сокровища ждут победителей!
humb1t



#мероприятия
https://paper.dropbox.com/doc/SITQuest-1--BQUUaNzTK~aa0R7CB9UuCjmLAQ-WJX7WHR8bvOKhoEMNEv4V