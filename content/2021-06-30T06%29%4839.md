+++
 title = "Middle/Senior C# разработчик"
 date = 2021-06-30
 description = "%D0%9A%D0%BE%D0%BD%D1%82%D1%83%D1%80%20-%20%D1%84%D0%B5%D0%B4%D0%B5%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%20%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE%20%D0%BE%D0%B1%D0%B5%D1%81%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D1%8F%20%D0%B4%D0%BB%D1%8F%20%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81%D0%B0%20-%20%D0%B2%20%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0%D1%85%20%D0%BE%D0%BF%D1%8B%D1%82%D0%BD%D0%BE%D0%B3%D0%BE%20C%23%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%20%D1%81%20%D0%BE%D0%BF%D1%8B%D1%82%D0%BE%D0%BC%20%D0%BF%D1%80%D0%BE%D0%BC%D1%8B%D1%88%D0%BB%D0%B5%D0%BD%D0%BD%D0%BE%D0%B9%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B8%20%D0%BE%D1%82%202%20%D0%BB%D0%B5%D1%82."
 [extra]
 preview_img ="/logo.jpg"
+++

Контур - федеральный разработчик программного обеспечения для бизнеса - в поисках опытного C# разработчика с опытом промышленной разработки от 2 лет.

Технологии и инструменты, используемые на проекте:
C# (.NET Core), API, React, Teamcity, Octopus, Vault, Git, YouTrack, Mongo DB
Дарья

sycheva.d@skbkontur.ru
telegram @darya_sycheva
#вакансии
https://perm.hh.ru/vacancy/45378619